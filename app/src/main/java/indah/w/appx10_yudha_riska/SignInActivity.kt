package indah.w.appx10_yudha_riska

import android.content.Intent
import android.os.Bundle

import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_signin.*

class SignInActivity :  AppCompatActivity() {

    var fbAuth = FirebaseAuth.getInstance()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signin)
        btnLogOff.setOnClickListener {
            fbAuth.signOut()
            val intent = Intent ( this, MainActivity::class.java)
            startActivity(intent)
        }
    }
}