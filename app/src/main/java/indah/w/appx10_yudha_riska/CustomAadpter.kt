package indah.w.appx10_yudha_riska

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.storage.StorageReference
import com.squareup.picasso.Picasso

class CustomAdapter(val context: Context,
                    arrayList: ArrayList<HashMap<String, Any>>) : BaseAdapter(), View.OnClickListener {

    val F_NAME = "file_name"
    val F_TYPE = "file_type"
    val F_URL = "file_url"
    val list = arrayList
    var uri = Uri.EMPTY
    lateinit var storage : StorageReference
    lateinit var db : CollectionReference
    lateinit var  alFile : ArrayList<HashMap<String,Any>>
    lateinit var adapter : CustomAdapter


    inner class ViewHolder(){
        var txFileName : TextView? = null
        var txFileType : TextView? = null
        var txFileUrl : TextView? = null
        var imv : ImageView? = null
        var id : TextView? = null
    }


    /**
     * Get a View that displays the data at the specified position in the data set. You can either
     * create a View manually or inflate it from an XML layout file. When the View is inflated, the
     * parent View (GridView, ListView...) will apply default layout parameters unless you use
     * [android.view.LayoutInflater.inflate]
     * to specify a root view and to prevent attachment to the root.
     *
     * @param position The position of the item within the adapter's data set of the item whose view
     * we want.
     * @param convertView The old view to reuse, if possible. Note: You should check that this view
     * is non-null and of an appropriate type before using. If it is not possible to convert
     * this view to display the correct data, this method can create a new view.
     * Heterogeneous lists can specify their number of view types, so that this View is
     * always of the right type (see [.getViewTypeCount] and
     * [.getItemViewType]).
     * @param parent The parent that this view will eventually be attached to
     * @return A View corresponding to the data at the specified position.
     */
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {
        var holder = ViewHolder()
        var view = convertView
        if (convertView == null){
            var inflater:LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)
                    as LayoutInflater
            view = inflater.inflate(R.layout.row_data, null, true)

            holder.txFileName = view!!.findViewById(R.id.txFileName) as TextView
            holder.txFileType = view!!.findViewById(R.id.txFileType) as TextView
            holder.txFileUrl = view!!.findViewById(R.id.txFileUrl) as TextView
            holder.imv = view!!.findViewById(R.id.imV) as ImageView

            view.tag = holder
        }else{
            holder = view!!.tag as ViewHolder
        }
        var fileType= list.get(position).get(F_NAME).toString()
        uri = Uri.parse(list.get(position).get(F_URL).toString())

        holder.txFileName!!.setText(list.get(position).get(F_NAME).toString())
        holder.txFileType!!.setText(fileType)
        holder.txFileUrl!!.setText(uri.toString())
        holder.txFileUrl!!.setOnClickListener{
            val intent = Intent(Intent.ACTION_VIEW).setData(
                Uri.parse(holder.txFileUrl!!.text.toString())
            )
            context.startActivities(arrayOf(intent))
        }
        when(fileType){
            ".pdf" -> {holder.imv!!.setImageResource(android.R.drawable.ic_dialog_dialer)}
            ".docx" -> {holder.imv!!.setImageResource(android.R.drawable.ic_menu_edit)}
            ".mp4" -> {holder.imv!!.setImageResource(android.R.drawable.ic_media_play)}
            ".jpg" -> {Picasso.get().load(uri).into(holder.imv)}
        }

        return view!!
    }

    override fun getItem(position: Int): Any {
        return list.get(position)
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getCount(): Int {
        return list.size
    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    override fun onClick(v: View?) {
        val intent = Intent()
        intent.action = Intent.ACTION_GET_CONTENT
        when(v?.id) {


            }
        }
    }




object db {

}
